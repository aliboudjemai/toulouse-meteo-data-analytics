fastapi==0.78.0
requests==2.27.1
fastapi-versioning==0.10.0
google-cloud-bigquery==3.1.0
google-cloud-storage==2.2.1
uvicorn[standard]
