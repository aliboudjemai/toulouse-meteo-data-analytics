from typing import Any, Dict

from google.cloud import bigquery


class BigQueryHelper:
    def __init__(self, client: bigquery.Client):
        self.client = client

    def get_max_timestamp(self, dst_table: Dict[str, str], station: str = None):
        query = f"""
        SELECT MAX(heure_de_paris) as max_ts from `{dst_table['project']}.{dst_table['dataset']}.{dst_table['table']}`
        WHERE {'station_id = @stationId' if station else '1=1'}
        """
        job_config = bigquery.QueryJobConfig(
            query_parameters=([
                bigquery.ScalarQueryParameter("stationId", "STRING", station)
            ] if station else []))
        job = self.client.query(query, job_config=job_config)
        for row in job.result():  # only one expected row
            return row["max_ts"]
        return None  # no data

    def append_data(self, data: Dict[str, Any], dst_table: Dict[str, str]):
        query = f"""
        INSERT INTO `{dst_table['project']}.{dst_table['dataset']}.{dst_table['table']}` (
            id, station_id, timestamp, data, humidite, direction_du_vecteur_de_vent_max, pluie_intensite_max, 
            pression, direction_du_vecteur_vent_moyen, type_de_station, pluie, direction_du_vecteur_de_vent_max_en_degres, 
            force_moyenne_du_vecteur_vent, force_rafale_max, temperature_en_degre_c, heure_de_paris, heure_utc
        )
        VALUES (
            '{data['id']}', 
            '{data['station_id']}', 
            '{data['timestamp']}', 
            '{data['fields']['data']}', 
            {data['fields']['humidite']}, 
            {data['fields']['direction_du_vecteur_de_vent_max']}, 
            {data['fields']['pluie_intensite_max']}, 
            {data['fields']['pression']}, 
            {data['fields']['direction_du_vecteur_vent_moyen']}, 
            '{data['fields']['type_de_station']}', 
            {data['fields']['pluie']}, 
            {data['fields']['direction_du_vecteur_de_vent_max_en_degres']}, 
            {data['fields']['force_moyenne_du_vecteur_vent']}, 
            {data['fields']['force_rafale_max']}, 
            {data['fields']['temperature_en_degre_c']}, 
            '{data['fields']['heure_de_paris']}', 
            '{data['fields']['heure_utc']}'
        );
        """
        query_job = self.client.query(query)
