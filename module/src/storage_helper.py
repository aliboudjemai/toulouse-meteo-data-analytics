"""Storage helper module."""
import datetime
from typing import Optional

from google.cloud import storage


class StorageHelper:
    """Helper for GCS access."""

    def __init__(
        self,
        storage_client: storage.Client,
    ) -> None:
        """Init helper."""
        self._storage_client = storage_client

    def upload(self, bucket_name: str, filename: str):
        bucket = self._storage_client.get_bucket(bucket_name)
        blob = bucket.blob(filename)
        blob.upload_from_filename(filename)
